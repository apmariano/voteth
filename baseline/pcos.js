const fs = require('fs');
const net = require('net');
const bitwise = require('bitwise')
const aes = require('aes256');

// The port number and hostname of the servers
const regional_port = 8080;
const regional_host = '10.0.80.174';
// const regional_host = 'localhost';
const transparency_port = 8081;
const transparency_host = '10.0.80.176';
// const transparency_host = 'localhost';

// Create new TCP clients.
const client_transp = new net.Socket();
const client_reg = new net.Socket();
const key = "This is the hecking key";

// Processing Time variables (Transparency)
var start_enc_transp;
var end_enc_transp;
var start_trans_transp;
var end_trans_transp;

// Processing Time variables (Regional)
var start_enc_reg;
var end_enc_reg;
var start_trans_reg;
var end_trans_reg;

// Create csv for storing the results
// fs.appendFileSync('results/pcos.csv', 'PCOS Batch,Encryption Time (PtoT),Transmission Time (PtoT),Encryption Time (PtoR),Transmission Time (PtoR)\n');
fs.appendFileSync('results/raw_ballots.txt', '\nBATCH\n\n');

function generateRandomBallot(){
    // Generated Ballot in Hex String
    var ballot = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    // Generate Random Votes for the President
    var pres = Math.random() * 100;

    if (0 <= pres && pres <= 0.06) { ballot[0] = 1 }
    if (0.06 < pres && pres <= 3.48) { ballot[1] = 1 }
    if (3.48 < pres && pres <= 24.85) { ballot[2] = 1 }
    if (24.85 < pres && pres <= 48.29) { ballot[3] = 1 }
    if (48.29 < pres && pres <= 61.01) { ballot[4] = 1 }
    if (61.01 < pres && pres <= 100) { ballot[5] = 1 }

    // Generate Random Votes for the President
    var vpres = Math.random() * 100;

    if (0 <= vpres && vpres <= 12.01) { ballot[6] = 1 }
    if (12.01 < vpres && vpres <= 13.93) { ballot[7] = 1 }
    if (13.93 < vpres && vpres <= 49.04) { ballot[8] = 1 }
    if (49.04 < vpres && vpres <= 51.16) { ballot[9] = 1 }
    if (51.16 < vpres && vpres <= 65.53) { ballot[10] = 1 }
    if (65.53 < vpres && vpres <= 100) { ballot[11] = 1 }

    fs.appendFileSync('results/raw_ballots.txt', ballot + "\n");
    return ballot;
}

var tally = new Array(12)
for (var i = 0; i < tally.length; i++) tally[i] = 0;

// Generate 20 random ballots, keeping the tally after each ballot
for (var i = 0; i < 225; i++){
    var ballot = generateRandomBallot();
    for (var j = 0; j < 12; j++) tally[j] += ballot[j];
}

console.log("Tally: ");
console.log(tally + "\n");

// Append tally to pcos.csv
fs.appendFileSync('results/pcos.csv', tally.join(' ') + ',');

// Send a connection request to the Transparency
client_transp.connect({ port: transparency_port, host: transparency_host }, function() {
    console.log('Connection established with Transparency Server.');

    // Start measuring time for encryption
    start_enc_transp = process.hrtime();
    
    // Actual AES encryption
    var encrypted = aes.encrypt(key, tally.toString());

    // End measuring time for encryption
    end_enc_transp = process.hrtime(start_enc_transp);
    console.log("Encryption Time: " + end_enc_transp[1]/1000000 + "ms");
    fs.appendFileSync('results/pcos.csv', end_enc_transp[1]/1000000 + ",");

    // Start measuring time for transmission
    start_trans_transp = process.hrtime();

    // Actual Transmission
    client_transp.write(encrypted);
    client_transp.end();

});

client_transp.on('end', function() {
    // End measuring time for transmission
    end_trans_transp = process.hrtime(start_trans_transp);
    console.log("Transparency Transmission Time: " + end_trans_transp[1]/1000000 + "ms\n");
    fs.appendFileSync('results/pcos.csv', end_trans_transp[1]/1000000 + ",");
})

setTimeout(function() {
    // Send a connection request to the Regional Server
    client_reg.connect({ port: regional_port, host: regional_host }, function() {
        console.log('Connection established with the Regional Server.');

        // Start measuring time for encryption
        start_enc_reg = process.hrtime();

        // Actual AES encryption
        var encrypted = aes.encrypt(key, tally.toString());

        // End measuring time for encryption
        end_enc_reg = process.hrtime(start_enc_reg);
        console.log("Encryption Time: " + end_enc_reg[1]/1000000 + "ms");
        fs.appendFileSync('results/pcos.csv', end_enc_reg[1]/1000000 + ",");


        // Start measuring time for transmission
        start_trans_reg = process.hrtime();

        // Actual Transmission
        client_reg.write(encrypted);
        client_reg.end();
    });

    client_reg.on('end', function() {
        // End measuring time for transmission
        end_trans_reg = process.hrtime(start_trans_reg);
        console.log("Regional Transmission Time: " + end_trans_reg[1]/1000000 + "ms\n");
        fs.appendFileSync('results/pcos.csv', end_trans_reg[1]/1000000 + "\n");
    })
}, 1000);