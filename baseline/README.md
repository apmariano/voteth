﻿# Baseline System

The **baseline architecture** exists to check if the performance of blockchain is better than that of the current Philippine electoral system. In this architecture, we are simulating the network structure of the electoral system in the Philippines through the simulation of the PCOS machines. The PCOS machine collates all the ballots, tallies the votes, and sends the tallied result to the COMELEC server and a transparency server.

For the baseline architecture, we used TCP as the network protocol to send the results from the PCOS machine to the COMELEC and Transparency servers. Moreover, we used `Node.js` to mimic the PCOS machines and the servers.

## System Files

 1. `pcos.js` --- Simulates the PCOS machine by randomly generating ballots, tallying ballots, encrypting results using Advanced Encryption Standard (AES), and transmitting the encrypted results. This Node.js file also serves as the client to both the Regional and Transparency servers.
 2. `regional.js` --- Simulates the Regional server by accepting and decrypting results from different PCOS machines, collating all results into a final tally, encrypting the final tally, and transmitting the final tally to the COMELEC server. The Regional server serves as both a server to the PCOS machines and a client to the COMELEC server.
 3. `comelec.js` --- Simulates the COMELEC server by accepting and decrypting the final tally from the Regional server. The COMELEC server also reports the winners of the election.
 4. `transparency.js` --- Simulates the Transparency server by accepting and decrypting the results from the PCOS machines and collating all results into a final tally. Similar to the COMELEC server, the Transparency server also reports the winners of the election.

## Experimentation Steps

 1. Set `num_pcos` in `regional.js` and `transparency.js` to the number of PCOS machines expected in the iteration of experimentation.
 2. Find a machine to run the COMELEC server and check its IP address. Change `final_host` in `regional.js` to the IP address of the machine running `comelec.js`. If applicable, change `final_port` to an unused port in that machine.
 3. Find a machine to run the transparency server and check its IP address. Change `transparency_host` in `pcos.js` to the IP address of the machine running `transparency.js`.  If applicable, change `transparency_port` to an unused port in that machine.
 4. Find a machine to run the regional server and check its IP address. Change `regional_host` in `pcos.js` to the IP address of the machine running `regional.js`.  If applicable, change `regional_port` to an unused port in that machine.
 5. Run the servers in separate machines in this particular order: `comelec.js`, `transparency.js`, and `regional.js`.
 6. In a separate machine, run	`pcos.js` multiple times depending on the expected number of PCOS machines in the iteration of experimentation.
 7. After the experimentation, rename the `results` folder to a distinct name to avoid overwriting of directories.
