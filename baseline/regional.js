const fs = require('fs');
const net = require('net');
const aes = require('aes256');
const key = "This is the hecking key";
const regional_port = 8080; // Port of the PCOS-Regional

// Port and Host for sending tally to COMELEC
const final_port = 8000; // Port of Regional-COMELEC
const final_host = '10.0.80.165';
// const final_host = 'localhost';

// Create a new TCP client.
const client = new net.Socket();

// Processing Time Variables
var start_dec;
var end_dec;
var start_enc;
var end_enc;
var start_trans;
var end_trans;
var start_tally;
var end_tally;

// Store the overall results
//  and expected number of PCOS Machines
//  and number of ballots sent
var overall_tally = [];
var num_pcos = 25; // SET THIS
var pcos_messaged = 0;

// Create csv for storing the results
fs.appendFileSync('results/regional.csv', 'PCOS Batch,Decryption Time (R),Tally Time (R)\n');
fs.appendFileSync('results/comelec_regional.csv', 'Final Batch,Encryption Time (RtoC),Transmission Time (RtoC)\n')

// Update the overall tally
function tallyVotes(array) {
    if (!overall_tally.length) overall_tally = Array(array.length).fill(0);
    for (var i = 0; i < overall_tally.length; i++) {
        overall_tally[i] += array[i];
    }
}

// What the server will do once someone connects
var server = net.createServer(function(c){

    c.on("data", function(chunk){

        // Start measuring time for decryption
        start_dec = process.hrtime();

        // Actual AES decryption
        var decrypted = aes.decrypt(key, chunk.toString())

        // End measuring time for decryption
        end_dec = process.hrtime(start_dec);

        // Start measuring time for tallying
        start_tally = process.hrtime();

        // Actual Tallying
        var vote_count = decrypted.split(",").map(Number);
        fs.appendFileSync('results/regional.csv', vote_count.join(' ') + ',');
        tallyVotes(vote_count);

        // Log the time for decryption
        console.log("Decryption Time: " + end_dec[1]/1000000 + "ms");
        fs.appendFileSync('results/regional.csv', end_dec[1]/1000000 + ",");

        // End measuring time for tallying
        end_tally = process.hrtime(start_tally);
        console.log("Tallying Time: " + end_tally[1]/1000000 + "ms");
        fs.appendFileSync('results/regional.csv', end_tally[1]/1000000 + "\n");
        
        console.log("Current Tally: " + overall_tally + "\n");
        pcos_messaged++;

        // If the PCOS tallies are complete,
        //  we now send the final tally to COMELEC
        if (pcos_messaged === num_pcos) {
            // Send a connection request to the COMELEC server
            client.connect({ port: final_port, host: final_host }, function() {
                console.log('Connection established with the COMELEC Server.');

                // Append final tally to comelec_regional.csv
                fs.appendFileSync('results/comelec_regional.csv', overall_tally.join(" ") + ",");
         
                // Start measuring time for encryption
                start_enc = process.hrtime();

                // Actual AES encryption
                var encrypted = aes.encrypt(key, overall_tally.toString());

                // End measuring time for encryption
                end_enc = process.hrtime(start_enc);
                console.log("Encryption Time: " + end_enc[1]/1000000 + "ms");
                fs.appendFileSync('results/comelec_regional.csv', end_enc[1]/1000000 + ",");

                // Start measuring time for transmission
                start_trans = process.hrtime();

                // Actual Transmission
                client.write(encrypted);
                client.end();
                 
            });

            client.on('end', function() {
                // End measuring time for transmission
                end_trans = process.hrtime(start_trans);
                console.log("COMELEC Transmission Time: " + end_trans[1]/1000000 + "ms\n");
                fs.appendFileSync('results/comelec_regional.csv', end_trans[1]/1000000 + "\n");
            });
        }
    });

    c.on("end", function(){

    });

});

server.listen(regional_port, function(){
    console.log("Regional Server Running!\n");
});