const fs = require('fs');
const net = require('net');
const aes = require('aes256');
const key = "This is the hecking key";
const final_port = 8000; // Port of the Regional-COMELEC

// Processing Time Variables
var start_dec;
var end_dec;

// Create csv for storing the results
fs.appendFileSync('results/comelec.csv', 'Decryption Time (C),President Winner,VP Winner\n');

function returnWinners(array){
    winner_pres = []
    winner_vpres = []

    winner_pres = array.slice(0,6).reduce(function(a, e, i) {
        if (e === Math.max.apply(Math, array.slice(0,6)))
            a.push(i);
        return a;
    }, []);

    winner_vpres = array.slice(6,12).reduce(function(a, e, i) {
        if (e === Math.max.apply(Math, array.slice(6,12)))
            a.push(i+6);
        return a;
    }, []);

    return [winner_pres, winner_vpres];
}

// What the server will do once someone connects
var server = net.createServer(function(c){

    c.on("data", function(chunk) {

        // Start measuring time for decryption
        start_dec = process.hrtime();

        // Actual AES decryption
        var decrypted = aes.decrypt(key, chunk.toString())

        // End measuring time for decryption
        end_dec = process.hrtime(start_dec);
        console.log("Decryption Time: " + end_dec[1]/1000000 + "ms");
        fs.appendFileSync('results/comelec.csv', end_dec[1]/1000000 + ",");

        var vote_count = decrypted.split(",").map(Number);
        console.log("Final Tally: " + vote_count + "\n");
        console.log("\n" + "President Winner: Candidate " + returnWinners(vote_count)[0] + "\nVice President Winner: Canidate " + returnWinners(vote_count)[1]);
        fs.appendFileSync('results/comelec.csv', "Candidate " + returnWinners(vote_count)[0] + "," + "Candidate " + returnWinners(vote_count)[1] + "\n");
    });

    c.on("end", function(){

    });
});

server.listen(final_port, function(){
    console.log("COMELEC Server Running!\n");
})