const fs = require('fs');
const net = require('net');
const aes = require('aes256');
const key = "This is the hecking key";
const transparency_port = 8081; // Port of the PCOS-Transparency

// Processing Time Variables
var start_dec;
var end_dec;
var start_tally;
var end_tally;

// Store the overall results
//  and expected number of PCOS Machines
//  and number of ballots sent
var overall_tally = [];
var num_pcos = 25; // set this
var pcos_messaged = 0;

// Create csv for storing the results
fs.appendFileSync('results/transparency.csv', 'PCOS Batch,Decryption Time (T),Tally Time (T)\n');

// Update the overall tally
function tallyVotes(array) {
    if (!overall_tally.length) overall_tally = Array(array.length).fill(0);
    for (var i = 0; i < overall_tally.length; i++) {
        overall_tally[i] += array[i];
    }
}

function returnWinners(array){
    winner_pres = []
    winner_vpres = []

    winner_pres = array.slice(0,6).reduce(function(a, e, i) {
        if (e === Math.max.apply(Math, array.slice(0,6)))
            a.push(i);
        return a;
    }, []);

    winner_vpres = array.slice(6,12).reduce(function(a, e, i) {
        if (e === Math.max.apply(Math, array.slice(6,12)))
            a.push(i+6);
        return a;
    }, []);

    return "President Winner: Candidate " + winner_pres + "\nVice President Winner: Canidate " + winner_vpres;
}

// What the server will do once someone connects
var server = net.createServer(function(c){

    c.on("data", function(chunk){

        // Start measuring time for decryption
        start_dec = process.hrtime();

        // Actual AES decryption
        var decrypted = aes.decrypt(key, chunk.toString())

        // End measuring time for decryption
        end_dec = process.hrtime(start_dec);

        // Start measuring time for tallying
        start_tally = process.hrtime();

        // Actual Tallying
        var vote_count = decrypted.split(",").map(Number);
        fs.appendFileSync('results/transparency.csv', vote_count.join(' ') + ',');
        tallyVotes(vote_count);

        // Log the time for decryption
        console.log("Decryption Time: " + end_dec[1]/1000000 + "ms");
        fs.appendFileSync('results/transparency.csv', end_dec[1]/1000000 + ",");

        // End measuring time for tallying
        end_tally = process.hrtime(start_tally);
        console.log("Tallying Time: " + end_tally[1]/1000000 + "ms");
        fs.appendFileSync('results/transparency.csv', end_tally[1]/1000000 + "\n");
        
        console.log("Current Tally: " + overall_tally + "\n");
        pcos_messaged++;

        // If the PCOS tallies are complete,
        //  we return the winners
        if (pcos_messaged === num_pcos) {
            console.log("\n" + returnWinners(vote_count));
        }
    });

    c.on("end", function(){

    });
});

server.listen(transparency_port, function(){
    console.log("Transparency Server Running!\n");
});