# Argument checker
if [ $# -lt 1 ]; then
	echo "ERR: First argument is the number of PCOS machines"
	exit 1
fi

MACHINES=$1

# Navigate to main
cd ..

##### SETTING UP THIRD PARTY NODES #####
for i in {1..10}; do
	# Navigate to the third-party's folder
	cd tp$(($i-1))
	# Create the Truffle folder
	mkdir truffle
	# Navigate to it
	cd truffle
	# Initialize the Truffle files
	truffle init
	# Copy files from truffle-third
	cp -R ../../truffle-third/. .
	# Get one thirdParty-X.js from comelec's /thirdparty-scripts
	cp ../../comelec/truffle/thirdparty-scripts/thirdParty-$i.js ./command
	# Copy build folder from COMELEC
	cp -R ../../comelec/truffle/build .
	# Return to main folder
	cd ../..
done

##### SETTING UP PCOS MACHINES #####
for (( i = 1; i <= $MACHINES; i++ )); do
	# Navigate to the PCOS machine folder
	cd pcos$(($i-1))
	# Create the Truffle folder
	mkdir truffle
	# Navigate to it
	cd truffle
	# Initialize Truffle files
	truffle init
	# Copy contents of /truffle-pcos
	cp -R ../../truffle-pcos/. .
	# Copy build folder
	cp -R ../../comelec/truffle/build .
	# Return to main directory
	cd ../..
done
