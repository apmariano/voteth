# Argument checker
if [ $# -lt 1 ]; then
	echo "ERR: First argument is the number of PCOS machines"
	exit 1
fi

MACHINES=$1

# Navigate to main directory
cd ..

# Check for existence of terminal-output, create otherwise
if [ ! -d "./terminal-output" ]; then
	mkdir terminal-output
fi

# Get COMELEC enode
printf 'admin.nodeInfo.enode' | ./geth.sh comelec 00 | awk "/enode/ {print}" | tee -a ./terminal-output/enode-addresses.txt

# Get Third-Party enodes
for i in {1..9}; do
	printf 'admin.nodeInfo.enode' | ./geth.sh tp$(($i-1)) 0$i | awk "/enode/ {print}" | tee -a ./terminal-output/enode-addresses.txt
done
printf 'admin.nodeInfo.enode' | ./geth.sh tp9 10 | awk "/enode/ {print}" | tee -a ./terminal-output/enode-addresses.txt

# Get PCOS enodes
for (( i = 1; i <= $MACHINES; i++ )); do
	printf 'admin.nodeInfo.enode' | ./geth.sh pcos$(($i-1)) $(($i+10)) | awk "/enode/ {print}" | tee -a ./terminal-output/enode-addresses.txt
done
