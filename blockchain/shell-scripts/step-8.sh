# Argument checker
if [ $# -lt 1 ]; then
	echo "ERR: First argument is the number of PCOS Machines"
	exit 1
fi

MACHINES=$1

# Navigate to main directory
cd ..

# Copy static-nodes.json to COMELEC
cp static-nodes.json ./comelec/geth

# Copy static-nodes.json to Third-Party
for i in {1..10}; do
	cp static-nodes.json ./tp$(($i-1))/geth
done

# Copy static-nodes.json to PCOS Nodes
for (( i = 1; i <+ $MACHINES; i++ )); do
	cp static-nodes.json ./pcos$(($i-1))/geth
done
