# Navigate to COMELEC's Truffle folder
cd ../comelec/truffle

# Compile smart contracts using Truffle
truffle compile

# Migrate the compilation
truffle migrate
