# Check second argument
if [ $# -lt 1 ]
then
	echo "ERR: Argument after ./step-1-1.sh should be the number of PCOS machines."
	exit 1
fi
MACHINES=$1
echo "Number of machines: $MACHINES"

# Navigate to main directory
pwd
cd ..
pwd

# Create COMELEC node
geth --datadir comelec init genesis.json

# Create THIRD-PARTY nodes
for i in {1..10}; do
	geth --datadir tp$(($i-1)) init genesis.json
done

# Create PCOS nodes
for ((i = 1; i <= $MACHINES; i++ ))
do
	geth --datadir pcos$((i-1)) init genesis.json
done
