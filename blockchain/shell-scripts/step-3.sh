# Navigate to the COMELEC folder
cd ../comelec

# Create the Truffle folder
mkdir truffle

# Navigate inside the Truffle folder
cd truffle

# Initialize the Truffle files
truffle init

# Copy the contents of truffle-comelec inside the Truffle folder
cp -R ../../truffle-comelec/. .
