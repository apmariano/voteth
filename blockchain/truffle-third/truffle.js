module.exports = {
  rpc: {
    host:"localhost",
    port:8545
  },
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: 2018,
      from: "",
      gas: 0x210000
    }
  }
 };