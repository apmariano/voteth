if [ $# -lt 2 ]; then
	echo "ERR: First argument should be the DIRECTORY and second argument is the PORT NUMBER (last TWO digits)"
	exit 1
fi

geth --datadir $1 --networkid 2018 --port 303$2 --nodiscover --rpc --rpcport 85$2 --rpcaddr 127.0.0.1 --rpccorsdomain "*" --rpcapi "eth,net,web3,personal,miner" console
