module.exports = {
  rpc: {
    host:"localhost",
    port:8544
  },
  networks: {
    development: {
      host: "localhost",
      port: 8544,
      network_id: 2018,
      from: "",
      gas: 0x210000
    }
  }
 };