var bitwise = require('bitwise')

module.exports = {
    // ------------------------------------ //
    //  Tally the votes bit-per-bit since each bit corresponds to a
    //  vote for a candidate
    //
    //  NOTE: needs some slight tweaking for != 12 candidates
    //
    // @param1: [String Array] allBallots = the array of decrypted votes
    // @param2: [number] numOfCandidates = the number of candidates in the ballot
    //
    // @return: [number Array] results = final tally of votes for each candidate
    //    
    // ------------------------------------ //
    countVote: function(_allBallots, _numOfCandidates){
        var results = Array(_numOfCandidates).fill(0);
        for (var i in _allBallots) {
            var hexBallot = Buffer.from(_allBallots[i] + '0', 'hex');
            var bits = bitwise.buffer.read(hexBallot).slice(0,12);
            for (var j in results) results[j] = results[j] + bits[j];
        }
        return results;
    }
}