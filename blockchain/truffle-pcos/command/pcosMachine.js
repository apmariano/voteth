// module.exports = function(callback){

// Include Nodejs' net module.
const Net = require('net');
// The port number and hostname of the server.
const port = 8080;
const host = 'localhost';

// Create a new TCP client.
const client = new Net.Socket();

var BitArray = require('node-bitarray')
var Threshold = require('./threshold')
var bitwise = require('bitwise')

var pubKey = "0x04505c9bd16b109ff9e4088e47cd61ddb992d4cde0da253d8b732bc9ad25815416085f30b2a3fce792c40dbf61f677f3e2dbc07bd7d35a7a1234a065f819dcea60";
pubKey = Buffer.from(pubKey.replace(/0x/, ""), 'hex');

var ballotCount = -1;

// Where we'll place the timer logs (for ballot encryption) and unencrypted ballots
// (for checking accuracy in EXPERIMENTATION)
var fs = require('fs');
var dir = './logs';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
var filename_time = Date.now();
var tally_stream = fs.createWriteStream(dir+"/tally-"+filename_time+".csv", {flags: 'a'});
var enc_stream = fs.createWriteStream(dir+"/ballot-encryption-"+filename_time+".csv", {flags: 'a'});
var encDetails_stream = fs.createWriteStream(dir+"/encrypted-ballot-contents-"+filename_time+".txt", {flags: 'a'});
tally_stream.write("Ballot No.,,Ballot Contents\n");
enc_stream.write("Ballot No.,Encryption Time (ms)\n");


function sendBallot(ballot){
    client.write(ballot);
}

async function generateBallot(){
    // Count how many ballots are already sent, stop once there are 625 ballots
    ballotCount += 1;
    if (ballotCount > 225){
        clearInterval(potato);
        client.end();
    }
    
    // Generated Ballot in Hex String
    var ballot = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    // Generate Random Votes for the President
    var pres = Math.random() * 100;

    if (0 <= pres && pres <= 0.06) { ballot[0] = 1 }
    if (0.06 < pres && pres <= 3.48) { ballot[1] = 1 }
    if (3.48 < pres && pres <= 24.85) { ballot[2] = 1 }
    if (24.85 < pres && pres <= 48.29) { ballot[3] = 1 }
    if (48.29 < pres && pres <= 61.01) { ballot[4] = 1 }
    if (61.01 < pres && pres <= 100) { ballot[5] = 1 }

    // Generate Random Votes for the President
    var vpres = Math.random() * 100;

    if (0 <= pres && pres <= 12.01) { ballot[6] = 1 }
    if (12.01 < pres && pres <= 13.93) { ballot[7] = 1 }
    if (13.93 < pres && pres <= 49.04) { ballot[8] = 1 }
    if (49.04 < pres && pres <= 51.16) { ballot[9] = 1 }
    if (51.16 < pres && pres <= 65.53) { ballot[10] = 1 }
    if (65.53 < pres && pres <= 100) { ballot[11] = 1 }

    // Convert the ballot into a hexadecimal
    // var ballotHex = BitArray.toHex(ballot)
    // if (ballotHex.length === 2) { ballotHex = ballotHex.concat('0') }
    ballotHex = bitwise.nibble.write(ballot.slice(0,4)).toString(16) + bitwise.nibble.write(ballot.slice(4,8)).toString(16) + bitwise.nibble.write(ballot.slice(8,12)).toString(16)

    // Add the ballot to the logs (for later checking) -- THIS IS FOR EXPERIMENTAL PURPOSES ONLY
    tally_stream.write(ballotCount+",,"+ballot.toString()+",,"+ballotHex+"\n");

    // This is where the asynchronous encryption takes place --
    // PROMISE should eventually encrypt and send the ballot.
    var hrstart = process.hrtime();
    return Threshold.encryptBallot(pubKey, ballotHex).then(function(encryptedBallot){
        var hrend = process.hrtime(hrstart);
        console.log("Ballot %d: %ds %dms", ballotCount, hrend[0], hrend[1]/1000000)
        enc_stream.write(ballotCount+","+hrend[0]*1000+hrend[1]/1000000+"\n");
        encDetails_stream.write("========================= BALLOT "+ballotCount+" DETAILS =========================\n");
        encDetails_stream.write("---------- JSON CONTENTS ----------\n");
        encDetails_stream.write(JSON.stringify(encryptedBallot[0])+"\n");
        encDetails_stream.write("---------- HEX EQUIVALENT ----------\n");
        encDetails_stream.write(encryptedBallot[1]+"\n\n");
        sendBallot(encryptedBallot[1]);
        console.log("Ballot "+ballotCount+" has been sent for transaction!");
        
    }, function(e){console.log(e);});
}


// Send a connection request to the server or
// What the client will do if it connects to the server.
client.connect({ port: port, host: host }, function() {
    // If there is no error, the server has accepted the request and created a new 
    // socket dedicated to us.
    console.log('TCP connection established with the server.');

    // The client can now send data to the server by writing to its socket.
    // client.end();
});

// The client can also receive data from the server by reading from its socket.
// What the client will do if it receives data from the server.
client.on('data', function(chunk) {
    console.log('Data received from the server: ');
    console.log(chunk);
    
    // Request an end to the connection after the data has been received.
    client.end();
});

// What the client will do while sending "FIN"
client.on('end', function() {
    console.log('Requested an end to the TCP connection');
});

var potato = setInterval(generateBallot, 1000);

// }