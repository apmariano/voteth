module.exports = function(callback){
    const artifacts1 = require('../build/contracts/Voting.json');
    const artifacts2 = require('../build/contracts/SecretSharing.json');
    const contract = require('truffle-contract');
    var Promise = require("bluebird");
    
    const Voting = contract(artifacts1);
    const SecretSharing = contract(artifacts2);
    const Threshold = require('./threshold');
    
    Voting.setProvider(web3.currentProvider);
    SecretSharing.setProvider(web3.currentProvider);

    const net = require('net')
    const port = 8080;

    var ballotQueue = [];
    var pubKey = "";
    var ballotCount = 0;

    // Where we'll place the timer logs (for mining)
    var fs = require('fs');
    var dir = './logs';
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    var mine_stream = fs.createWriteStream(dir+"/mining-"+Date.now()+".csv", {flags: 'a'});
    mine_stream.write("Ballot No.,Mining* Time (ms),, Gas Used, Cumulative Gas Used,, Txn, Block Hash, Block Number, Contract Address, From, Logs, Logs Bloom, Root, To, Txn Hash, Txn Index\n")

    // Function for creating transaction
    async function makeTransaction(){
        var copy = ballotQueue;
        var copyCount = new Array(copy.length)
        for (var i=0; i<copyCount.length; i++) copyCount[i] = ballotCount + i;
        ballotCount += copy.length;
        
        ballotQueue = [];

        // 
        return Voting.deployed().then(function(instance){
            voting = instance;
        }).then(function(){
            // Send each encrypted ballot in their own transaction
            return Promise.map(copy, function(ballot, index){
                var hrstart = process.hrtime()
                return voting.sendVote(ballot, {from: web3.eth.accounts[0], gas: 3000000})
                .then(function(result){
                    var hrend = process.hrtime(hrstart)
                    console.log("Ballot %d: %ds %dms",copyCount[index], hrend[0], hrend[1]/1000000)
                    mine_stream.write(copyCount[index]+","+hrend[0]+hrend[1]/1000000+",,"+result.receipt.gasUsed+","+result.receipt.cumulativeGasUsed+",,")
                    mine_stream.write(result.tx+","+result.receipt.blockHash+","+result.receipt.blockNumber+","+result.receipt.contractAddress+","+result.receipt.from+",")
                    mine_stream.write(result.receipt.logs+","+result.receipt.logsBloom+","+result.receipt.root+","+result.receipt.to+","+result.receipt.transactionHash+",")
                    mine_stream.write(result.receipt.transactionIndex+"\n")
                },function(err){
                    console.log(err);
                });
            });
        });
    }

    
    /** ACTUAL FLOW STARTS BELOW **/
    
    // Retrieve public key
    SecretSharing.deployed().then(function(instance){
        ss = instance;
        return ss.getPublicKey.call();
    })

    // Remove the '0x' from the publicKey and convert to hex Buffer in preparation for encryptBallot().
    .then(function(result){
        console.log(result);
        pubKey = Buffer.from(result.replace(/0x/, ""), 'hex');
        console.log("Public Key is prepared! ");
    }, function(err){
        console.log(err);
    })
    
    // Creating the server where the PCOS machine can connect
    var server = net.createServer(function(c){
        console.log("Server connected!");
    
        // Once data is received, decrypt it and add it to the queue
        c.on("data", function(chunk){
            var ballotData = chunk.toString();
            if ((ballotData.match(/0x/g) || []).length > 1){
                var singleBallotData = ballotData.split("0x");
                for (var i = 1; i < singleBallotData.length; i++) ballotQueue.push("0x"+singleBallotData[i])
            }
            else{
                ballotQueue.push(ballotData);
            }
        })
    
        // When the PCOS machine ends its transaction
        c.on("end", function(){
            console.log("Server disconnected");
        })
    })

    // Start listening to PCOS machines
    server.listen(port, function(){
        console.log("Server bound!");
    });
    
    // Make transactions every 5 seconds
    console.log("Interval set!")
    var potato = setInterval(makeTransaction, 5000);
}