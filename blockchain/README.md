# Voteth: Applying Blockchains to the Current Philippine Elections
This is the instructional guide on setting up the blockchain system using these source files. Take note that this guide was done with **Truffle v4.1.15** and **Node v8.9.4**.

## (Pre-election) Using Shell Scripts
Make sure that all scripts (except for geth.sh) have been `chmod +x` and are in the shell-scripts directory.

### Creating the Ethereum Nodes
1. Run `./step-1.sh <pcos_machines>` (where pcos_machines is the number of PCOS machines) to create Ethereum accounts for each node.
2. Edit the `alloc` portion of genesis.json to add all the nodes' Eth addresses (found in `./terminal-output/eth-addresses.txt` - don't add `0x` to the address)
3. Run `./step-2.sh <pcos_machines>` (where pcos_machines is the number of PCOS machines) to initialize each node.

Initiate a Geth instance using `./geth.sh <folder> <id>` (where `folder`is the relevant folder name and `id` is the last **TWO-digit** port number [00-99])

### Setting up COMELEC Smart Contracts
This section mostly focuses on the COMELEC node. It assumes that two terminals are running: one for the node's geth instance and another one for the shell scripts.

1. Run `./step-3.sh` to set-up the Truffle files and folder inside COMELEC node.
2. Edit `truffle.js` so that it uses the same port and address as the COMELEC node.
	- The fields to edit specifically are the `port`(which should be the same rpcport) and the `from` (which should be the node's Ethereum address).
3. Add all the PCOS Node addresses to `Rules.sol` under the `/truffle/contracts` folder.
	- Make sure that these addresses are properly checksummed. Use https://ethsum.netlify.com/ to properly checksum if it is not.
4. In the node's geth console, run `personal.unlockAccount(<address>, <password>, 0)` to allow the node to send transactions. Run `miner.start()` to allow the node to start mining.
5. In the other console, run `./step-4.sh` to compile and migrate the smart contracts to the blockchain.
6. Run `miner.stop()` to stop the mining process.

### Generate Public and Shared Keys
1. In `generateKeys.js` line 26, change the parameter of `Threshold.keyGeneration()` to the number of COMELEC and Third-Party Nodes in the set-up.
2. Unlock the COMELEC Node using `personal.unlockAccount` and start mining (`miner.start`).
3. Run `./step-5.sh` to generate keys.
4. A `publickey.txt` should be generated. Paste its contents into the pubKey variable in `truffle-pcos/pcosMachine.js`'s line 16.

### Setting up the other nodes' Smart Contracts
1. Run `./step-6.sh <pcos_machines>` (where pcos_machines is the number of PCOS Machines) to set-up the files and folders of the Third-Party and PCOS nodes.
2. Edit `truffle.js`so that it uses the same address as the COMELEC node and the port of the node.
	- Do this for **all** the Third-Party and PCOS nodes.
	- The fields to edit specifically are the `port` (which should be the same rpcport as the running node) and the `from` (which should be the COMELEC node's Ethereum address).
- Edit the `port` variable of `newGenerateVotes.js` and `pcosMachine.js` as necessary if running multiple instances in the same computer.

### Connecting the Nodes Together
Make sure that no geth instance is running anywhere.
1. Run `./step-7.sh <pcos_machines>` (where pcos_machines is the number of PCOS Machines) to generate the enode addresses.
2. Replace line 2 of `static-nodes.json` with all the enode addresses (found in `/terminal-output/enode-addresses.txt`).
3. Place the `static-nodes.json` in the `geth` folders of COMELEC + Third-Party nodes.



## (Pre-election) Without Shell Scripts
### Creating the Ethereum nodes

1. Create an Ethereum account by running `geth --datadir <folder> account new`. **Copy the resulting address somewhere safe.**
	- This step must be done to all the nodes that will be used, especially the non-miner nodes (PCOS Node) before proceeding to the next step
	- The `<folder>` should be different each time it is run -- it's going to house the blockchain files of a node. This folder will be referred to as the nodes' main folder in this document.
2. Edit the `alloc` portion of genesis.json to add all the nodes' addresses.
	- The `difficulty` parameter is edited here as well.
3. Initialize each node by running `geth --datadir <folder> init genesis.json`.
	- Make sure the `<folder>`s are the same as the ones generated in Step 1.
	- ~~At this point, the nodes' main folder and `genesis.json` can be copied over to other computers. They will be run on that computer now. *But let's not do this yet until the everything has been fully set up.*~~
4. Start the node by running `geth --datadir <folder> --networkid 2018 --port <port> --nodiscover --rpc --rpcport 8543 --rpcaddr 127.0.0.1 --rpccorsdomain "*" --rpcapi "eth,net,web3,personal,miner" console`.

### Setting up COMELEC Smart Contracts

This section mostly focuses on the COMELEC node. It assumes that two terminals are running: one for the node's geth instance and the other one for the truffle command.

1. On the COMELEC folder, create the Truffle folder (`mkdir truffle`) and navigate to it (`cd truffle`).
2. Run `truffle init` to initialize the folder.
3. Copy the contents of `/truffle-comelec` inside of the node's Truffle folder.
4. Edit `truffle.js` so that it uses the same port and address as the COMELEC node.
	- The fields to edit specifically are the `port`(which should be the same rpcport) and the `from` (which should be the node's Ethereum address).
5. Add all the PCOS Node addresses to `Rules.sol` under the `/truffle/contracts` folder.
6. In the node's geth console, run `personal.unlockAccount(<address>, <password>, 0)` to allow the node to send transactions. Run `miner.start()` to allow the node to start mining.
7. In the other console (which is still inside the Truffle folder), run `truffle compile` and `truffle migrate` to compile and deploy the smart contract onto the blockchain.
	- The node can stop mining for the meantime using `miner.stop()` if preferred.

### Generate Public and Shared Keys
1. In `generateKeys.js` line 26, change the parameter of `Threshold.keyGeneration()` to the number of COMELEC and Third-Party Nodes in the set-up.
2. Unlock the COMELEC Node using `personal.unlockAccount` and start mining (`miner.start`).
3. Run `truffle exec command/generateKeys.js`

A file called `publicKey.txt` and directory `thirdparty-scripts` has been made along with some files. These will be relevant later.

### Setting up the other nodes' Smart Contracts
For this section, the geth instance of the node does not have to be active yet.
1. Navigate to the desired node's folder.
2. Create the Truffle folder (`mkdir truffle`) and navigate to it (`cd truffle`).
3. Run `truffle init` to initialize the folder.
4. Copy the contents of the relevant folder inside of the node's Truffle folder.
	- It is `/truffle-third` for Third-Party nodes and `/truffle-pcos` for the PCOS nodes.
5. For the Third-Party nodes, copy one file (`thirdparty-X.js`) from the COMELEC node's `/thirdparty-scripts` folder into the `command` folder.
	- No two nodes should have the same file.
	- One of those `thirdParty-X.js` files is for the COMELEC node.
6. For the PCOS nodes, edit the `pcosMachine.js` pubKey variable in line 15 to match the value in the generated `publicKey.txt` earlier.
6. Edit `truffle.js`so that it uses the same address as the COMELEC node and the port of the node.
	- The fields to edit specifically are the `port` (which should be the same rpcport as the running node) and the `from` (which should be the COMELEC node's Ethereum address).
7. Copy the `build` folder from COMELEC's `truffle`folder.
8. Repeat Steps 1--7 for all nodes.

At this point, these files are ready to be moved over to other computers as necessary.

### Connecting the Nodes Together
This step requires all nodes' (COMELEC, Third-Party, PCOS) geth instances to be up.
1. Get the enode addresses of each node by running `admin.nodeInfo.enode` on the geth console. **Copy these enode addresses somewhere safe**.
2. For the COMELEC and Third-Party node, run `admin.addPeer(<enode address>)` to add all the other enode addresses besides its own enode address.
	- Change the `[::]` into the appropriate IP address if on another computer.
	- There is **no need** for the PCOS node to do this.
3. Run `admin.peers` to check the list of peers a node has.



## Election Simulation
### COMELEC and Third-Party Nodes
All they will do at this point is just to mine any incoming transactions from the PCOS Node. Remember that their Geth instances are unlocked with `personal.unlockAccount`.

### PCOS Node
The PCOS Node consists of its Geth instance and its Truffle instance. The Geth instance must also bu unlocked.

1.  Run `truffle exec command/newGenerateVotes.js` to simulate the PCOS Node which will receive the votes from the PCOS Machine.

### PCOS Machine
1. Make sure that the PCOS Node is running before running `node pcosMachine.js`.

Once all votes have been sent, the elections is over. `endOfElections` may be used to restrict time of voting, but for ease of use in experimentation, it is not used.

## Post-Election Phase
### Reconstructing the Private Key
1. At the end 

## Useful code snippets
Put this in `.bashrc` to make multi-tab console slightly easier. Invoke `set-title "<title>"` in the Terminal to change the header title.
```
set-title(){
  PS1="\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "
  PROMPT_COMMAND=
  echo -en "\033]0;$@\a"
}
```

This is the relevant code snippets once the system's files has been set up, so finding these in the above chapters won't be such a hassle anymore.

`geth --datadir <folder> --networkid 2018 --port <port> --nodiscover --rpc --rpcport 8543 --rpcaddr 127.0.0.1 --rpccorsdomain "*" --rpcapi "eth,net,web3,personal,miner" console`
```
personal.unlockAccount(<address>, <password>, 0)
miner.start()
admin.nodeInfo.enode
admin.addPeer()
admin.peers
```