var Voting = artifacts.require("./Voting.sol");
var SecretSharing = artifacts.require("./SecretSharing.sol");

module.exports = function(deployer) {
  deployer.deploy(Voting);
  deployer.deploy(SecretSharing);
};
