module.exports = function(callback){
    const artifacts1 = require('../build/contracts/Voting.json');
    const artifacts2 = require('../build/contracts/SecretSharing.json');
    const contract = require('truffle-contract');
    var Promise = require("bluebird");

    const Voting = contract(artifacts1);
    const SecretSharing = contract(artifacts2);
    const Threshold = require('./threshold');
    const Tally = require('./tallying');
    var BitArray = this.require('node-bitarray');

    Voting.setProvider(web3.currentProvider);
    SecretSharing.setProvider(web3.currentProvider);

    var prKey;
    var voting, ss;
    var numOfVotes;
    var numOfKeys;
    var decryptedVotes;
    var results;

    // Where we'll place the timer logs (for ballot decryption and tallying)
    var fs = require('fs');
    var dir = './logs';
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    var filename_time = Date.now();
    var tally_stream = fs.createWriteStream(dir+"/tallying-"+filename_time+".csv", {flags: 'a'});
    var dec_stream = fs.createWriteStream(dir+"/ballot-decryption-"+filename_time+".csv", {flags: 'a'});
    dec_stream.write("Ballot No.,Decryption Time (ms)\n");

    // Get the private key from blockchain
    Voting.deployed().then(function(instance){
        voting = instance;
        // Get the number of shared keys in SecretSharing
        return SecretSharing.deployed().then(function(instance){
            ss = instance;
            console.log("Get number of keys...");
            return ss.getNumberOfKeys.call();
        })
        // Get all the shared keys in the sharedKeys array
        .then(function(result){
            console.log("NUMBER OF KEYS: "+result);
            numOfKeys = Array(result);
            for (var i = 0; i < result; i++) numOfKeys[i] = i;
            return Promise.map(numOfKeys, function(i){
                return ss.getSharedKey.call(i);
            })
            // Process the input by removing the '0x0' for each shared key
            // before inserting it in the combineKeys() function
            .then(function(result){
                for (var i in result) result[i] = result[i].replace(/0x0/, "");
                return Threshold.combineKeys(result);
            }, function(err){
                console.log(err);
            })
        }, function(err){
            console.log(err);
        })
    })
    // Convert the private key hex string into a Buffer
    .then(function(result){
        console.log("Convert private key into buffer...");
        console.log("PRIVATE KEY: "+result);
        prKey = Buffer.from(result.replace(/0x/, ""), 'hex');
    }, function(err){
        console.log(err);
    })
    // Get how many votes there are on the Voting s.c.
    .then(function(){
        return voting.getNumberOfVotes.call()
    })
    // and construct an array of [0 ~ n-1] for use of Promise.map
    .then(function(result){
        console.log("NUMBER OF VOTES: "+result);
        numOfVotes = Array(result);
        for (var i = 0; i < result; i++) numOfVotes[i] = i;
    }, function(err){
        console.log(err);
    })
    // Retrieve the encrypted votes from the blockchain
    .then(function(){
        return Promise.map(numOfVotes, function(i){
            return voting.getVote.call(i);
        })
    })
    // Convert every retrieved encrypted vote into Buffer
    .then(function(result){
        decryptedVotes = result;
        result.forEach(function(val, idx){
            result[idx] = Buffer.from(val.replace(/0x/, ""), 'hex');
        });
        // Decrypt all the encrypted ballots
        return Promise.map(result, function(encVote, index){
            var dec_hrstart = process.hrtime();
            return Threshold.decryptBallot(prKey, encVote).then(function(result){
                var dec_hrend = process.hrtime(dec_hrstart);
                console.log("Ballot "+index+": "+dec_hrend[0]+dec_hrend[1]/1000000);
                dec_stream.write("Ballot "+index+": "+dec_hrend[0]+dec_hrend[1]/1000000+"\n");
                return result;
            })
        })
        // Save the decrypted votes in an array
        .then(function(result){
            decryptedVotes = result;
        }, function(err){
            console.log(err);
        })
    }, function(err){
        console.log(err);
    })
    // Display all the decrypted votes
    // and tally the votes in a results array
    .then(function(){
        console.log();
        decryptedVotes.forEach(function(val, idx){
            var bits = BitArray.fromHex(val);
            var ballot = bits.toJSON();
            for (var i = ballot.length; i < 12; i++) ballot.push(0);
            console.log("DECRYPTED VOTE "+idx+": "+val);
            console.log(ballot);
        })
        var tally_hrstart = process.hrtime();
        results = Tally.countVote(decryptedVotes, 12);
        var tally_hrend = process.hrtime(tally_hrstart);
        console.log("Tallying Time: "+tally_hrend[0]+tally_hrend[1]/1000000);
        tally_stream.write("Tallying Time: "+tally_hrend[0]+tally_hrend[1]/1000000+"\n");
        console.log();
        for (var i in results) console.log("CANDIDATE "+i+": "+results[i]+" VOTES");
        tally_stream.write(results.toString());
    })
}