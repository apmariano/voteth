module.exports = function(callback){
const artifacts1 = require('../build/contracts/Voting.json');
const artifacts2 = require('../build/contracts/SecretSharing.json');
const contract = require('truffle-contract');
var Promise = require("bluebird");

const Voting = contract(artifacts1);
const SecretSharing = contract(artifacts2);
const Threshold = require('./threshold');

Voting.setProvider(web3.currentProvider);
SecretSharing.setProvider(web3.currentProvider);

// Where we'll place the generated shared keys
// NOTE: We will not simulate transmission of shared keys in the experimentation
var fs = require('fs');
var dir = './thirdparty-scripts';
if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}

/** GENERATE KEYS AND PUBLISH RELEVANT KEYS TO THE BLOCKCHAIN */

// Generate keys
// Change the parameter to the number of (COMELEC + Third-Parties)
    result = Threshold.keyGeneration(2);

// Publish public and shared keys onto blockchain

    // '0x' is appended to the start of publicKey when deployed
    // '0x0' is appended to each share to become 50 bytes.
    var publicKey = "0x"+result.publicKey.toString('hex');
    var shares = result.shares;
    for (var i in shares) shares[i] = "0x0"+shares[i];

    console.log("SHARES");
    console.log(shares);

    // Set public key
    SecretSharing.deployed().then(function(instance){
        ss = instance;
        return ss.setPublicKey(publicKey, {from: web3.eth.accounts[0], gas: 200000});
    })
    // Retrieve the value of public key from the blockchain
    .then(function(result){
        return ss.getPublicKey.call();
    }).then(function(result){
        console.log("PUBLIC RESULT : "+result);
    }, function(err){
        console.log(err);
    })

    // Writes the entire shared key array to a text file to be hardcoded
    .then(function(){
        const tpp1 = "module.exports = function(callback){\n\tconst artifacts2 = require('../build/contracts/SecretSharing.json');\n\tconst contract = require('truffle-contract');\n\tconst SecretSharing = contract(artifacts2);\n\tSecretSharing.setProvider(web3.currentProvider);\n\tconst sharedKey = '"
        const tpp2 = "';\n\tSecretSharing.deployed().then(function(instance){\n\t\tss = instance;\n\t\tss.addSharedKey(sharedKey, {from:web3.eth.accounts[0], gas: 200000});\n\t})\n}"
        
        var filename = './command/thirdParty-comelec.js';
        var stream = fs.createWriteStream(filename);
        stream.write(tpp1+shares[0]+tpp2);
        stream.end();

        for (var i = 1; i < shares.length; i++){
            var filename = './thirdparty-scripts/thirdParty-' + i + '.js';
            var stream = fs.createWriteStream(filename);
            stream.write(tpp1+shares[i]+tpp2);
            stream.end();
        }
        var stream = fs.createWriteStream("./publicKey.txt");
        stream.write(publicKey);
        stream.end();
        console.log("Text file has been created!");
    })
    
    
}