module.exports = function(callback){

    // Where we'll place the timer logs (for ballot encryption) and unencrypted ballots
    // (for checking accuracy in EXPERIMENTATION)
    var fs = require('fs');
    var dir = './logs';
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    var blockchain_stream = fs.createWriteStream(dir+"/blockchain-"+Date.now()+".csv", {flags: 'a'});
    blockchain_stream.write("Block Number,,Difficulty, Extra Data, Gas Limit, Gas Used, Hash, Logs Bloom, Miner, Mix Hash, Nonce, Number, Parent Hash, Receipt Root, SHA3 Uncles, Size (bytes), State Root, Timestamp, Total Difficulty, Transactions Root,,Transactions,,Uncles\n");
    var blockHeight = web3.eth.blockNumber;
    for (var i = 0; i <= blockHeight; i++){
        var block = web3.eth.getBlock(i);
        blockchain_stream.write(i+",,"+block.difficulty+","+block.extraData+","+block.gasLimit+","+block.gasUsed+","+block.hash+","+block.logsBloom+","+block.miner+","+block.mixHash+","+block.nonce+","+block.number+","+block.parentHash+","+block.receiptsRoot+","+block.sha3Uncles+","+block.size+","+block.stateRoot+","+block.timestamp+","+block.totalDifficulty+","+block.transactionsRoot+",,"+block.transactions+",,----------,,"+block.uncles+"\n")
        console.log("Scraped block "+i+"!");
    }
}