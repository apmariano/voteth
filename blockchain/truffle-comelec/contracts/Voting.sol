pragma solidity ^0.4.24;

import "./Rules.sol";

contract Voting is Rules {

    bytes[] ballots;

    function getVote(uint _index) external view returns(bytes) {
        return ballots[_index];
    }
    
    function getNumberOfVotes() external view returns(uint) {
        return ballots.length;
    }

    function sendVote(bytes _encryptedBallot) external onlyPCOS {
        ballots.push(_encryptedBallot);
    }

    function returnTimeStamp() external view returns (uint){
        return block.timestamp;
    }

}