pragma solidity ^0.4.24;

import "./Rules.sol";

contract SecretSharing is Rules {

    bytes[] sharedKeys;
    
    function addSharedKey(bytes _sharedKey) external {
        sharedKeys.push(_sharedKey);
    }

    function getNumberOfKeys() external view returns(uint)  {
        return sharedKeys.length;
    }
    
    function getSharedKey(uint _index) external view returns(bytes) {
        return sharedKeys[_index];
    }
    
}