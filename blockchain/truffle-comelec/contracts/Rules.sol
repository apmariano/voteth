pragma solidity ^0.4.24;

contract Rules {

    address private _owner;
    address[] PCOS; // = [0xFFF, 0xAFF]
    uint startTime;
    uint endTime;

    bytes publicKey;
    
    constructor() internal {
        _owner = msg.sender;
        startTime = 0;
        endTime = 1;
    }
    
    function owner() public view returns(address) {
        return _owner;
    }
    
    modifier onlyOwner() {
        require(
            msg.sender == _owner,
            "Sender not authorized."
        );
        _;
    }
    
    //...//...manually input PCOS address is the only way...//...//
    modifier onlyPCOS() {
        require(
            msg.sender == 0x,
            "Sender is not a PCOS node."
        );
        _;
    }
    
    modifier duringElections() {
        require(
            block.timestamp >= startTime && block.timestamp < endTime,
            "Election period is over."
        );
        _;
    }
    
    modifier endOfElections() {
        require(
            block.timestamp >= endTime,
            "Election period is still ongoing."
        );
        _;
    }
    
    function getVoteTimePeriod() external view returns(uint, uint) {
        return (startTime, endTime);
    }

    function setStartTime(uint _time) external onlyOwner {
        startTime = _time;
    }

    function setEndTime(uint _time) external onlyOwner {
        endTime = _time;
    }

    function setPublicKey(bytes _publicKey) external onlyOwner {
        publicKey = _publicKey;
    }

    function getPublicKey() external view returns (bytes) {
        return publicKey;
    }
}